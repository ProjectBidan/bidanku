import React, {useEffect, useState} from 'react';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {ILHospitalBG} from '../../assets';
import {ListHospital} from '../../component';
import {Fire} from '../../config';
import {colors, fonts} from '../../utils';

const Hospitals = () => {
  const [hospitals, setHospitals] = useState([]);
  useEffect(() => {
    Fire.database()
      .ref('hospitals/')
      .once('value')
      .then((res) => {
        if (res.val()) {
          setHospitals(res.val());
        }
      });
  }, []);
  return (
    <View style={styles.page}>
      <ImageBackground source={ILHospitalBG} style={styles.background}>
        <Text style={styles.title}>Rumah Sakit di Kota Bengkulu</Text>
      </ImageBackground>
      <ScrollView style={styles.content}>
        {hospitals.map((item) => {
          return (
            <ListHospital
              key={item.id}
              type={item.type}
              name={item.name}
              address={item.address}
              pic={item.pic}
            />
          );
        })}
      </ScrollView>
    </View>
  );
};

export default Hospitals;
const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.secondary,
    flex: 1,
  },
  background: {
    height: 240,
    paddingTop: 12,
  },
  content: {
    backgroundColor: colors.white,
    borderRadius: 20,
    flex: 1,
    marginTop: -12,
    paddingTop: 14,
  },
  title: {
    fontSize: 24,
    maxWidth: 187,
    marginLeft: 16,
    fontFamily: fonts.primary[600],
    color: colors.text.primary2,
  },
});
