import React, {useEffect} from 'react';
import {ImageBackground, StyleSheet, Text} from 'react-native';
import {ILLogo, ILSplash} from '../../assets';
import {colors, fonts} from '../../utils';
import {Fire} from '../../config';

const Splash = ({navigation}) => {
  useEffect(() => {
    const unsubscribe = Fire.auth().onAuthStateChanged((user) => {
      setTimeout(() => {
        if (user) {
          navigation.replace('MainApp');
        } else {
          navigation.replace('GetStarted');
        }
      }, 3000);
    });

    return () => unsubscribe();
  }, [navigation]);

  return (
    <ImageBackground style={styles.page} source={ILSplash}>
      <ILLogo />
      <Text style={styles.title}>Bidan-Ku</Text>
    </ImageBackground>
  );
};

export default Splash;
const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    marginTop: 20,
  },
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
