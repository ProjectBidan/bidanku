import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Header, List} from '../../component';
import {Fire} from '../../config';
import {colors} from '../../utils';

const ChooseBidan = ({navigation, route}) => {
  const [listBidan, setListBidan] = useState([]);
  const itemCategory = route.params;
  useEffect(() => {
    callBidanByCategory(itemCategory.category);
  }, [itemCategory.category]);

  const callBidanByCategory = (category) => {
    Fire.database()
      .ref('bidans/')
      .orderByChild('category')
      .equalTo(category)
      .once('value')
      .then((res) => {
        if (res.val()) {
          const oldData = res.val();
          const data = [];
          Object.keys(oldData).map((item) => {
            data.push({
              id: item,
              data: oldData[item],
            });
          });
          setListBidan(data);
        }
      });
  };
  return (
    <View style={styles.page}>
      <Header
        type="dark"
        title={`Bidan ${itemCategory.category}`}
        onPress={() => navigation.goBack()}
      />
      {listBidan.map((bidan) => {
        return (
          <List
            key={bidan.id}
            type="next"
            profile={{uri: bidan.data.photo}}
            name={bidan.data.fullName}
            desc={bidan.data.gender}
            onPress={() => navigation.navigate('BidanProfile', bidan)}
          />
        );
      })}
    </View>
  );
};

export default ChooseBidan;
const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
});
