import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {ILGetStarted, ILLogoMini} from '../../assets';
import {Button, Gap} from '../../component';
import {colors, fonts} from '../../utils';

const GetStarted = ({navigation}) => {
  return (
    <ImageBackground source={ILGetStarted} style={styles.page}>
      <View style={styles.MiniHeader}>
        <Text style={styles.title}>Konsultasi Online Bersama Bidan</Text>
        <ILLogoMini />
      </View>
      <View style={styles.Button}>
        <Button
          title="Daftar Akun"
          onPress={() => navigation.navigate('Register')}
        />
        <Gap height={16} />
        <Button
          title="Masuk / Login"
          type="secondary"
          onPress={() => navigation.replace('Login')}
        />
      </View>
    </ImageBackground>
  );
};

export default GetStarted;
const styles = StyleSheet.create({
  page: {
    paddingVertical: 16,
    paddingHorizontal: 30,
    justifyContent: 'space-between',
    backgroundColor: colors.imageBackground,
    flex: 1,
  },
  title: {
    fontSize: 24,
    color: colors.white2,
    fontWeight: '600',
    textAlign: 'center',
    maxWidth: 196,
    marginTop: 45,
    marginRight: 80,
    fontFamily: fonts.primary[600],
  },
  MiniHeader: {
    flexDirection: 'row',
  },
  Button: {
    paddingHorizontal: 10,
  },
});
