import React from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {Button, Gap, Header, Profile, ProfileItem} from '../../component';
import {colors} from '../../utils';

const BidanProfile = ({navigation, route}) => {
  const dataBidan = route.params;
  return (
    <View style={styles.page}>
      <Header title="Profil Bidan" onPress={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <Profile
          name={dataBidan.data.fullName}
          desc={dataBidan.data.profession}
          photo={{uri: dataBidan.data.photo}}
        />
        {/* Kenapa 10?? karena dikurangin untuk paddingnya nanti yaitu 16 */}
        <Gap height={10} />
        <ProfileItem label="Alumnus" value={dataBidan.data.university} />
        <ProfileItem
          label="Tempat Praktik"
          value={dataBidan.data.hospital_address}
        />
        <ProfileItem label="No. STR" value={dataBidan.data.str_number} />
        <View style={styles.action}>
          <Button
            title="Mulai Konsultasi"
            onPress={() => navigation.navigate('Chatting', dataBidan)}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default BidanProfile;
const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
  action: {
    paddingHorizontal: 40,
    paddingTop: 23,
  },
});
