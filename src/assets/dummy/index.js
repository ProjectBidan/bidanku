import DummyUser from './users.png';
import DummyBidan1 from './Bidan1.png';
import DummyBidan2 from './Bidan2.png';
import DummyBidan3 from './BIdan3.png';
import DummyBidan4 from './Bidan4.png';
import DummyBidan5 from './Bidan5.png';
import DummyNews1 from './news1.png';
import DummyNews2 from './news2.png';
import DummyNews3 from './news3.png';
import DummyHospital1 from './hospital1.png';
import DummyHospital2 from './hospital2.png';
import DummyHospital3 from './hospital3.png';
export {
  DummyUser,
  DummyBidan1,
  DummyBidan2,
  DummyBidan3,
  DummyBidan4,
  DummyBidan5,
  DummyNews1,
  DummyNews2,
  DummyNews3,
  DummyHospital1,
  DummyHospital2,
  DummyHospital3,
};
