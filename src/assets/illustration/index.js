import ILLogo from './logo.svg';
import ILLogoMini from './mini-logo.svg';
import ILLogoMedium from './logo-medium.svg';
import ILSplash from './background-splash.png';
import ILGetStarted from './background-getstarted1.png';
import ILNullPhoto from './null-photo.png';
import ILCatCempaka from './cat-bid-cempaka.svg';
import ILCatMelayu from './cat-bid-melayu.svg';
import IlCatMuara from './cat-bid-muara.svg';
import ILCatAgung from './cat-bid-agung.svg';
import ILCatSamban from './cat-bid-samban.svg';
import ILHospitalBG from './hospital-background.png';
import ILOnBoarding1 from './Ilustrasi_Onboarding1.png';
import ILOnBoarding2 from './Ilustrasi_Onboarding2.png';
import ILOnBoarding3 from './Ilustrasi_Onboarding3.png';
export {
  ILLogo,
  ILLogoMini,
  ILLogoMedium,
  ILSplash,
  ILGetStarted,
  ILNullPhoto,
  ILCatCempaka,
  ILCatMelayu,
  IlCatMuara,
  ILCatAgung,
  ILCatSamban,
  ILHospitalBG,
  ILOnBoarding1,
  ILOnBoarding2,
  ILOnBoarding3,
};
