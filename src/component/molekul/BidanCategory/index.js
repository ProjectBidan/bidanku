import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {
  ILCatAgung,
  ILCatCempaka,
  ILCatMelayu,
  IlCatMuara,
  ILCatSamban,
} from '../../../assets';
import {colors, fonts} from '../../../utils';
const BidanCategory = ({category, onPress}) => {
  const Icon = () => {
    if (category === 'gading cempaka') {
      return <ILCatCempaka style={styles.illustration} />;
    }
    if (category === 'kampung melayu') {
      return <ILCatMelayu style={styles.illustration} />;
    }
    if (category === 'muara bangkahulu') {
      return <IlCatMuara style={styles.illustration} />;
    }
    if (category === 'ratu agung') {
      return <ILCatAgung style={styles.illustration} />;
    }
    if (category === 'ratu samban') {
      return <ILCatSamban style={styles.illustration} />;
    }
    return <ILCatCempaka style={styles.illustration} />;
  };
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Icon />
      <Text style={styles.label}>Kecamatan</Text>
      <Text style={styles.category}>{category}</Text>
    </TouchableOpacity>
  );
};

export default BidanCategory;
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.cardLight,
    borderRadius: 10,
    marginRight: 10,
    width: 100,
    height: 130,
  },
  illustration: {
    marginBottom: 15,
  },
  wrapperCategory: {
    marginHorizontal: -23,
  },
  label: {
    fontSize: 11,
    fontFamily: fonts.primary[300],
    color: colors.text.primary,
    maxWidth: 87,
  },
  category: {
    fontSize: 11,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    textTransform: 'capitalize',
  },
});
